function* gen_printCount() {
  for (var count = 1; count < 3; count++) {
    yield count;
  }
}

function* gen_delegatePrint() {
  yield console.log('Before delegation');
  yield *gen_printCount();
  yield console.log('After delegation');
}

module.exports = gen_delegatePrint;