var gen = require('../es6-generators');
var stdout = require("test-console").stdout;

module.exports = {
  'Test Generator': function(beforeExit, assert) {
    var g = gen();

    assert.isDefined(g, 'Generator is defined');

    var inspect = stdout.inspect();
    g.next();
    inspect.restore();
    assert.eql(inspect.output, ['Before delegation\n'], 'Generator first iteration correct');

    assert.eql(g.next(), {
      value: 1,
      done: false
    }, 'Generator delegation successful after one next iteration');

    assert.eql(g.next(), {
      value: 2,
      done: false
    }, 'Delegated function works properly');

    var once = false;
    beforeExit(function() {
      if (once) {
        return;
      }
      once = true;
      var inspect = stdout.inspect();
      g.next();
      inspect.restore();
      assert.eql(inspect.output, ['After delegation\n'], 'Control returns to calling function after works in delegated function completed');
    });
  }
};